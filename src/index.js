import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import Realtime from "./Realtime";

console.log(
  "Starting " +
    process.env.REACT_APP_NAME +
    " | " +
    process.env.REACT_APP_VERSION
);
console.log("Connecting to API: " + process.env.REACT_APP_REST_API);

ReactDOM.render(
  <React.StrictMode>
    <App />
    <hr></hr>
    <Realtime />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
